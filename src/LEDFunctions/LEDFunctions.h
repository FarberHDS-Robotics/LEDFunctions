#ifdef LEDFunctions_h
#define LEDFunctions_h

#include "Arduino.h"

class LEDFunctions
{
  public:
    LEDFunctions(int startPin, int endPin);
    void allOn();
    void allOff();

  private:
    int _startPin;
    int _endPin;
};

#endif // LEDFunctions_h