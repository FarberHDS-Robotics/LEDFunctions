#include "Arduino.h"
#include "LEDFunctions.h"

LEDFunctions::LEDFunctions(int startPin, int endPin)
{
    for (int i = startPin; i <= endPin; i++)
    {
        pinMode(i, OUTPUT);
    }

    _startPin = startPin;
    _endPin = endPin;
}

void LEDFunctions::allOn()
{
    for (int i = _startPin; i <= _endPin; i++)
    {
        digitalWrite(i, HIGH);
    }
}

void LEDFunctions::allOff()
{
    for (int i = _startPin; i <= _endPin; i++)
    {
        digitalWrite(i, LOW);
    }
}
